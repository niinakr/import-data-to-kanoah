# xlrd usage, see: http://scienceoss.com/read-excel-files-from-python/
# encoding=utf8
#import sys
#reload(sys)
#sys.setdefaultencoding('utf8')
#import xlrd
import requests
import config
from requests.auth import HTTPBasicAuth
import argparse
from openpyxl import load_workbook
import json

    
    
KANOAH_SEARCH_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testcase/search'
KANOAH_TESTCASE_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testcase'
KANOAH_TESTRUN_URL = config.JIRA_BASE_URL + '/rest/kanoahtests/1.0/testrun'

def get_json_or_raise(response):
    response.raise_for_status()
    return response.json()


def parseTest_result(filename):
    # grab the active worksheet
    wb = load_workbook(filename)

    ws = wb.active
    testCaseNames = []
    for row in ws.iter_rows('A{}:A{}'.format(ws.min_row,ws.max_row)):
        for cell in row:
            if cell.value is not None and cell.row != 1:
                result = {'Name': cell.value, 'Status': ws['C'+ str(cell.row)].value}
                testCaseNames.append(result)
                
                
    return testCaseNames   



def create_test_cases(project_key, test_cases, auth):
    test_results = []
    for i, entry in enumerate(test_cases):
                name = entry['Name']
                status = entry['Status']
                query = 'projectKey = "{}" AND name = "{}"'.format(project_key, name)
                params = {'query': query, 'fields': 'key,name'}
                response = requests.get(KANOAH_SEARCH_URL, params=params, auth=auth, verify=False)
                content = get_json_or_raise(response)

                # Get the test case by the exact name (Kanoah returns partial matches)
                test_case = next((item for item in content if item['name'] == name), None)
                if not test_case:
                    # Create missing test case on Kanoah
                    payload = {'projectKey': project_key, 'name': name}
                    response = requests.post(KANOAH_TESTCASE_URL, json=payload, auth=auth, verify=False)
                    test_case = get_json_or_raise(response)
                    
                key = test_case['key']
                result = {'testCaseKey': key, 'status': status}
                test_results.append(result)
                
    return test_results

def create_test_run(project_key, name, test_cases, auth):
    """Create a test run with the given test cases on Kanoah."""
    payload = {'projectKey': project_key, 'name': name, 'items': test_cases}
    response = requests.post(KANOAH_TESTRUN_URL, json=payload, auth=auth, verify=False)
    run_id = get_json_or_raise(response)['key']
    count = len(test_cases)
    print('Test run {} created ({} {})'.format(run_id, count, 'test' if count == 1 else 'tests'))
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str, help='Name of the Robot Framework output XML file')
    parser.add_argument('project', type=str, help='JIRA project key')
    parser.add_argument('testrun', type=str, help='Name of the test run')
    args = parser.parse_args()
    

    auth = HTTPBasicAuth(config.USERNAME, config.PASSWORD)
    test_cases = parseTest_result(args.file)
    test_results = create_test_cases(args.project, test_cases, auth)
    create_test_run(args.project, args.testrun, test_results, auth)

    
if __name__ == "__main__":
    main()
